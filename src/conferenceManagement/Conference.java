package conferenceManagement;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import lombok.Getter; //Use Library lombok
import lombok.Setter; //Use Library lombok
import java.text.SimpleDateFormat;

/**
 * 
 * @since 10/09/2020
 * @version 1.0
 * Conference class, the corresponding calculations are made, such as; morning and afternoon sessions, networking event and lightning conversations.
 */

@Getter @Setter
public class Conference {
 
    private List<Track> trackTalk;
    private int totalTrackMinutes;
    private int countTrack;
    private int countTalks;

    //Constructot default, inicialize valores
    public Conference() {
        this.trackTalk = new ArrayList<>(); //The track list is initialized
        this.totalTrackMinutes = 0; //The other attributes of the object are initialized to avoid errors in the calculations.
        this.countTalks = 0;
        this.countTrack = 0;
    }

    /**
     * Constructor ordinario, receives parameters and updates variables
     * @param trackTalk
     * @param totalTrackMinutes
     * @param countTrack
     * @param countTalks 
     */
    public Conference(List<Track> trackTalk, int totalTrackMinutes, int countTrack, int countTalks) {
        this.trackTalk = trackTalk;
        this.totalTrackMinutes = totalTrackMinutes;
        this.countTrack = countTrack;
        this.countTalks = countTalks;
    }
    
    /**
     * It allows to open the conference file and calculate the total minutes
     * @param fileName 
     */
    public void readFileTranConference(String fileName){
        
        //1. Declaration of local variables
        FileInputStream fileInput = null;
        String readLine;
        int accountant = 0 ,intMinutes = 0, totalMinutes = 0, noOfTracks = 0;        
       
        //2. The text file in read mode opens
        try {
            fileInput = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        //3. It is stored in the buffer to read the data
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(fileInput));
        
        System.out.println("Test input: \n");
      
        //4. The file is read as long as it is not the end of the line
        try {
            while ((readLine = bufferReader.readLine()) != null) {
                
                accountant +=1 ;                
                //5. Print data line for line
                System.out.println(readLine);
                
                //6. The title of each line of the file is extracted until the last space
                String title = readLine.substring(0, readLine.lastIndexOf(" "));      
                
                //7. The last word of each line of the file is obtained
                String timeMinutesConference = readLine.substring(readLine.lastIndexOf(" ") + 1);
                
                //8. You only get the number that corresponds to the minutes
                String minutes = readLine.replaceAll("\\D+", "");
                
                //9. If there is a lightning conference, 5 min is counted
                if("lightning".compareTo(timeMinutesConference) == 0 )
                {                 
                    intMinutes = 5;
                    totalMinutes += intMinutes;
                }else{
                    intMinutes = Integer.parseInt(minutes);
                    totalMinutes += intMinutes;
                }

                //10. create the object
                Track singleTalk = new Track(intMinutes, title);

                //11. Add a Talk Object, Fill all the input values
                trackTalk.add(singleTalk);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("totalMinutes: "+totalMinutes);
        //12. Set the total no. of count talks.
        this.countTalks = accountant;

        //13. set total no. of minutes of talks.
        this.totalTrackMinutes = totalMinutes;

        //14. Calculate the no. of tracks      
        double numberOfTracks =  totalMinutes/Configuration.TOTAL_CONFERENCE_TALKS_TRACK_MINUTES;         
        double fractionalPart = numberOfTracks % 1;       
        double integralPart = numberOfTracks - fractionalPart;
        int leftMinutes = totalMinutes - (int)integralPart*Configuration.TOTAL_CONFERENCE_TALKS_TRACK_MINUTES.intValue();
        
        if (leftMinutes > 0) noOfTracks = (int) integralPart + 1;
        else noOfTracks = (int) integralPart;
       
        this.countTrack = noOfTracks;

        //15. Close file
        try {
            bufferReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }       
        System.out.println("\n");
    }
    
    /***
     * It allows to generate the sessions according to the hours allowed in the morning and afternoon
     * @param trackCountIndex
     * @param trackTalks
     * @param trackCount
     * @param startTalkIndex
     * @param totalTalkCount
     * @return 
     */
    public int generateSessionConference(int trackCountIndex, List<Track> trackTalks, int trackCount,int startTalkIndex , int totalTalkCount){

        //Format hours
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a");
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR, 9);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.AM_PM, Calendar.AM);

        int sum180 = Configuration.MORNING_TIME_MINUTES;
        int sum240 = Configuration.AFTERNOON_TIME_MINUTES;

        int TalkIndex;

        String sessionTime;
        String SessionTitle;

        for(TalkIndex=startTalkIndex; TalkIndex< totalTalkCount;TalkIndex++) {

            // Get the combination of 180 and fill it
            if (sum180 >= trackTalks.get(TalkIndex).getMinutes()) {
                sum180 = sum180 - trackTalks.get(TalkIndex).getMinutes();
                sessionTime = sdf.format(cal.getTime()) + " " + trackTalks.get(TalkIndex).getTitle() + " " + trackTalks.get(TalkIndex).getMinutes() + "min";
                trackTalks.get(TalkIndex).setTitle(sessionTime);
                cal.add(Calendar.MINUTE, trackTalks.get(TalkIndex).getMinutes());
                SessionTitle = "Track" + " " + (trackCountIndex + 1);
                trackTalks.get(TalkIndex).setTrackTitle(SessionTitle);
            }
            if (sum180 < trackTalks.get(TalkIndex).getMinutes())
                break;

            if (sum180 > 0)
                continue;

            if (sum180 <= 0)
                break;
        }

        trackTalks.get(TalkIndex).setLunchFlag(true);
        sessionTime = "12:00 PM" + " " + "Lunch";
        trackTalks.get(TalkIndex).setLunchTitle(sessionTime);
        cal.add(Calendar.MINUTE, 60);

        TalkIndex++;

        for(;TalkIndex< totalTalkCount;TalkIndex++) {
            // Get the combination of 240 and fill it
            if (sum240 >= trackTalks.get(TalkIndex).getMinutes()) {
                sum240 = sum240 - trackTalks.get(TalkIndex).getMinutes();
                sessionTime = sdf.format(cal.getTime()) + " " + trackTalks.get(TalkIndex).getTitle() + " " + trackTalks.get(TalkIndex).getMinutes() + "min";
                trackTalks.get(TalkIndex).setTitle(sessionTime);
                cal.add(Calendar.MINUTE, trackTalks.get(TalkIndex).getMinutes());
                SessionTitle = "Track" + " " + (trackCountIndex + 1);
                trackTalks.get(TalkIndex).setTrackTitle(SessionTitle);
            }
            if (sum240 < trackTalks.get(TalkIndex).getMinutes())
                break;

            if (sum240 > 0)
                continue;

            if (sum240 <= 0)
                break;
        }

        if(totalTalkCount == (TalkIndex))
            --TalkIndex;
        trackTalks.get(TalkIndex).setNetworkingFlag(true);
        sessionTime = "5:00 PM" + " " + "Networking Event";
        trackTalks.get(TalkIndex).setNetworkingTitle(sessionTime);

        
        TalkIndex++;
        return TalkIndex;

    }
     
    /***
     * Print for console result
     * @param trackTalks 
     */
    public void printSessionTrackConference(List<Track> trackTalks){

        System.out.println("Test Output: \n");
        String TrackTitle = "xxxxxxxxxxxxxx";

        // Output the talks into tracks based on the totalTalks and the count of Talks.
        for(int trackCountIndex=0;trackCountIndex<trackTalks.size();trackCountIndex++)
        {
            // Print the Track Title
            if(!TrackTitle.equals(trackTalks.get(trackCountIndex).getTrackTitle()))
            {
                System.out.println(trackTalks.get(trackCountIndex).getTrackTitle()+ ":");
                System.out.println("\n");
                TrackTitle = trackTalks.get(trackCountIndex).getTrackTitle();
            }

            // Print the prepared talk's title for this Track
            System.out.println(trackTalks.get(trackCountIndex).getTitle());

            // if lunch flag set then output the Lunch Title
            if(trackTalks.get(trackCountIndex).isLunchFlag())
            {
                System.out.println(trackTalks.get(trackCountIndex).getLunchTitle());
            }

            // if networking flag set then output the Networking Title
            if(trackTalks.get(trackCountIndex).isNetworkingFlag())
            {
                System.out.println(trackTalks.get(trackCountIndex).getNetworkingTitle());
                // simple convention to display extra lines.
                System.out.println("\n");                
            }
        }
    } 
}
