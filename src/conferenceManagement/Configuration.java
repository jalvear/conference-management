
package conferenceManagement;

/**
 *
 * @author Jose Alvear
 */
public class Configuration {
    public static final int MORNING_TIME_MINUTES = 180; //three hours in the morning
    public static final int AFTERNOON_TIME_MINUTES = 240;//Four hours in the morning
    public static final Double TOTAL_CONFERENCE_TALKS_TRACK_MINUTES = 420.0;
    public static final String TALKS_INPUT_FILE = "ConferenceTrack.txt";
}
