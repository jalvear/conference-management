
package conferenceManagement;

/**
 *
 * @author Jose Alvear
 */
public class Main {
    
    public static void main(String [] args){
        
        //1. Instance object Conference
        Conference conference = new Conference();
        
        //2. The file is loaded with the conference session data
        conference.readFileTranConference(Configuration.TALKS_INPUT_FILE);
               
        int startTalkIndex = 0;

        //3. Schedule the Talks into Tracks.
        for(int trackCount = 0; trackCount < conference.getCountTrack(); trackCount++)
        {
            startTalkIndex = conference.generateSessionConference(trackCount, conference.getTrackTalk(), conference.getCountTrack(), startTalkIndex, conference.getCountTalks());           
        }

        //4. Output the schedule of Talks into tracks.
        conference.printSessionTrackConference(conference.getTrackTalk());
      
    }
}
