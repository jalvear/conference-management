package conferenceManagement;

import lombok.Getter; //Use Library lombok
import lombok.Setter;

/**
 * @author Jose Alvear
 * @since 10/09/2020
 * @version 1.0
 * Class Track
 */
@Getter @Setter //Anotacion que evita el codigo chatarra
public class Track {
    
    //Declaration of variables
    private int minutes;
    private boolean lunchFlag;
    private boolean networkingFlag;
    private String title;
    private String networkingTitle;
    private String sessionTime;
    private String lunchTitle;
    private String trackTitle;
    
    /***
     * Contructor for default, Initialize Boolean variables to false
     */
    public Track() {
        this.networkingFlag = false;
        this.lunchFlag = false;
    }
    
    /***
     * Contructor ordinario, receives parameters and updates variables
     * @param minutes
     * @param id
     * @param title 
     */
    public Track(int minutes, String title) {
        this.minutes = minutes;
        this.title = title;
    }
    
    /***
     * Contructor ordinario, receives parameters and updates variables
     * @param minutes    
     * @param lunchFlag
     * @param networkingFlag
     * @param title
     * @param networkingTitle
     * @param sessionTime
     * @param luchTitle 
     */
    public Track(int minutes, boolean lunchFlag, boolean networkingFlag, String title, String networkingTitle, String sessionTime, String luchTitle) {
        this.minutes = minutes;
        this.lunchFlag = lunchFlag;
        this.networkingFlag = networkingFlag;
        this.title = title;
        this.networkingTitle = networkingTitle;
        this.sessionTime = sessionTime;
        this.lunchTitle = luchTitle;
    }    

    @Override
    public String toString() {
        return "Track{" + "minutes=" + minutes + ", lunchFlag=" + lunchFlag + ", networkingFlag=" + networkingFlag + ", title=" + title + ", networkingTitle=" + networkingTitle + ", sessionTime=" + sessionTime + ", luchTitle=" + lunchTitle + '}';
    }
    
}
